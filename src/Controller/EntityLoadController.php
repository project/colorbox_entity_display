<?php

namespace Drupal\colorbox_entity_display\Controller;

use Drupal\Core\Asset\LibraryDiscoveryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Routing\Router;
use Drupal\Core\State\StateInterface;
use Drupal\path_alias\AliasManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Controller to load content entities and return results via JSON.
 */
class EntityLoadController extends ControllerBase {

  /**
   * Path Alias Manager service.
   *
   * @var \Drupal\path_alias\AliasManager
   */
  protected $pathAliasManager;

  /**
   * Router service.
   *
   * @var \Drupal\Core\Routing\Router
   */
  protected $router;

  /**
   * Entity Display Repository service.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * Renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Library discovery service.
   *
   * @var \Drupal\Core\Asset\LibraryDiscoveryInterface
   */
  protected $libraryDiscovery;

  /**
   * EntityLoadController constructor.
   *
   * @param \Drupal\path_alias\AliasManager $pathAliasManager
   *   Path Alias Manager service.
   * @param \Drupal\Core\Routing\Router $router
   *   Router service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity Type Manager service.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entityDisplayRepository
   *   Entity Display Repository service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   Renderer service.
   * @param \Drupal\Core\Asset\LibraryDiscoveryInterface $libraryDiscovery
   *   Library Discovery service.
   * @param \Drupal\Core\State\StateInterface $state
   *   State key/value store.
   */
  public function __construct(
    AliasManager $pathAliasManager,
    Router $router,
    EntityTypeManagerInterface $entityTypeManager,
    EntityDisplayRepositoryInterface $entityDisplayRepository,
    RendererInterface $renderer,
    LibraryDiscoveryInterface $libraryDiscovery,
    StateInterface $state
  ) {
    $this->pathAliasManager = $pathAliasManager;
    $this->router = $router;
    $this->entityTypeManager = $entityTypeManager;
    $this->entityDisplayRepository = $entityDisplayRepository;
    $this->renderer = $renderer;
    $this->libraryDiscovery = $libraryDiscovery;
    $this->stateService = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('path_alias.manager'),
      $container->get('router.no_access_checks'),
      $container->get('entity_type.manager'),
      $container->get('entity_display.repository'),
      $container->get('renderer'),
      $container->get('library.discovery'),
      $container->get('state')
    );
  }

  /**
   * Load and render entity specified in request path.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The incoming HTTP request.
   * @param string $entity_path
   *   The URL path for the entity (alias or system path).
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   JSON response containing rendered entity with css and js file paths.
   */
  public function entityLoad(Request $request, string $entity_path) {

    // No path specified? 404.
    if (empty($entity_path)) {
      throw new NotFoundHttpException();
    }

    // The path processor had to replace / with : in the entity path.
    $entity_path = preg_replace('/\:/', '/', $entity_path);
    $system_path = $this->pathAliasManager->getPathByAlias($entity_path);
    $params = $this->router->match($system_path);

    // No match? 404.
    if (empty($params)) {
      throw new NotFoundHttpException();
    }

    // Get entity from route parameters.
    $entity = NULL;
    foreach ($params as $param) {
      if ($param instanceof EntityInterface) {
        $entity = $param;
        break;
      }
    }

    // No entity? 404.
    if (empty($entity)) {
      throw new NotFoundHttpException();
    }

    // Not allowed to view? Access denied.
    if (!$entity->access('view')) {
      throw new AccessDeniedHttpException();
    }

    // Render the entity using default view mode.
    // @todo provide a way to specify a view mode.
    $entity_type = $entity->getEntityTypeId();
    $view_modes = array_keys($this->entityDisplayRepository->getViewModes($entity_type));
    $view_builder = $this->entityTypeManager->getViewBuilder($entity_type);
    if (!empty($view_builder)) {
      if (in_array('colorbox', $view_modes)) {
        $view_mode = 'colorbox';
      }
      else {
        $view_mode = 'full';
      }
      $render_array = $view_builder->view($entity, $view_mode);
      $build = $render_array;
      if ($entity->getEntityTypeId() == 'paragraphs_library_item') {
        // Workaround for view mode problem.
        $build = $view_builder->build($render_array);
        if (!empty($build['paragraphs'][0]) && !empty($build['#view_mode'])) {
          $build['paragraphs'][0]['#view_mode'] = $build['#view_mode'];
        }
      }
      $output = $this->renderer->render($build);

      // Query string to gain control over browser caching of CSS/JS files.
      $query_string = '?' . $this->stateService->get('system.css_js_query_string', '0');

      // Capture attached CSS/JS.
      // Skip core/drupal.
      $processed_libraries = ['core/drupal'];
      $css = [];
      $js = [];
      $process_library = function ($library, &$processed_libraries, &$css, &$js) use (&$process_library, $query_string) {
        if (!in_array($library, $processed_libraries)) {
          preg_match('/(.*)\/(.*)/', $library, $match);
          $info = $this->libraryDiscovery->getLibraryByName($match[1], $match[2]);
          if (!empty($info['css'])) {
            foreach ($info['css'] as $css_item) {
              if ($css_item['type'] == 'file') {
                $path = base_path() . $css_item['data'] . $query_string;
                if (!in_array($path, $css)) {
                  $css[] = $path;
                }
              }
              // @todo support external.
            }
          }
          if (!empty($info['js'])) {
            foreach ($info['js'] as $js_item) {
              if ($js_item['type'] == 'file') {
                $path = base_path() . $js_item['data'] . $query_string;
                if (!in_array($path, $js)) {
                  $js[] = $path;
                }
              }
              // @todo support external.
            }
          }
          if (!empty($info['dependencies'])) {
            foreach ($info['dependencies'] as $library) {
              $process_library($library, $processed_libraries, $css, $js);
            }
          }
          $processed_libraries[] = $library;
        }
      };
      if (!empty($build['#attached']['library'])) {
        foreach ($build['#attached']['library'] as $library) {
          $process_library($library, $processed_libraries, $css, $js);
        }
      }

      $result = [
        'css' => $css,
        'js' => $js,
        'markup' => $output,
      ];

      return new JsonResponse($result);
    }
  }

}
