// Display linked entities in a colorbox.

(function ($, Drupal) {

  Drupal.behaviors.colorboxEntityDisplay = {
    attach: function (context, settings) {
      if (context === document) {
        $('a.colorbox-display:not(.colorbox-display-processed)', context).on('click', function (e) {
          var $this = $(this);
          $this.addClass('colorbox-display-processed');
          var href = $this.attr('href');
          var hostmatch = window.location.protocol + '//' + window.location.host;
          if (href.startsWith(hostmatch)) {
            href = href.substr(hostmatch.length);
          } else if (href.startsWith('http')) {
            // Not supported for external links.
            return;
          }

          // Load content in a colorbox.
          e.preventDefault();
          attachManager.jsFileLoaded = 0;
          attachManager.cboxComplete = false;
          var loadUrl = '/colorbox-entity-display/entity-load' + href;
          $.colorbox({
            open: true
          });
          $.get(loadUrl, function (data) {
            attachManager.jsFileCount = data.js.length;
            for (var i = 0; i < data.css.length; i++) {
              attachJsCss(data.css[i], 'css');
            }
            for (i = 0; i < data.js.length; i++) {
              attachJsCss(data.js[i], 'js');
            }
            $.colorbox({
              html: data.markup,
              maxWidth: '90%',
              maxHeight: '90%',
              scrolling: true
            });
          }).fail(function () {
            window.location.href = href;
          });
          return false;
        });
      }
      if (!attachManager.cboxCompleteBound) {
        $(document).bind('cbox_complete', function () {
          attachManager.cboxComplete = true;
          attachManager.doAttach();
        });
        attachManager.cboxCompleteBound = true;
      }
    }
  };

  attachManager = {
    jsFileCount: 0,
    jsFileLoaded: 0,
    cboxCompleteBound: false,
    cboxComplete: false,
    doAttach: function () {
      if (this.jsFileLoaded >= this.jsFileCount && this.cboxComplete) {
        let context = document.getElementById('cboxContent');
        Drupal.attachBehaviors(context);
      }
    }
  }

  function attachJsCss(filename, filetype) {
    if (filetype == "js") {
      // Skip if already attached.
      var scripts = document.getElementsByTagName('script');
      for (var i = 0; i < scripts.length; i++) {
        var src = scripts[i].getAttribute('src');
        if (filename === src) {
          // Already exists.
          attachManager.jsFileCount--;
          attachManager.doAttach();
          return;
        }
      }
      var fileref = document.createElement('script');
      fileref.setAttribute("type","text/javascript");
      fileref.setAttribute("src", filename);
      fileref.addEventListener('load', function (event) {
        attachManager.jsFileLoaded++;
        attachManager.doAttach();
      });
      document.getElementsByTagName("body")[0].appendChild(fileref);
    }
    else if (filetype == "css") {
      var links = document.getElementsByTagName('link');
      for (var i = 0; i < links.length; i++) {
        var href = links[i].getAttribute('href');
        if (filename === href) {
          // Already exists.
          return;
        }
      }
      var fileref = document.createElement("link");
      fileref.setAttribute("rel", "stylesheet");
      fileref.setAttribute("type", "text/css");
      fileref.setAttribute("href", filename);
      document.getElementsByTagName("head")[0].appendChild(fileref);
    }
  }
})(jQuery, Drupal);
