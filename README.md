#Colorbox Entity Display

The Colorbox Entity Display module allows you to display content entities in a Colorbox.
The Colorbox module (https://www.drupal.org/project/colorbox) is required.

Once you install and enable this module, to display a content entity in a Colorbox,
in your theme template, create a link with the class "colorbox-display". For example:

`<a href="/path/to/my/entity" class="colorbox-display">See my content in a colorbox!</a>`

