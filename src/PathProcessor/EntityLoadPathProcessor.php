<?php

namespace Drupal\colorbox_entity_display\PathProcessor;

use Drupal\Core\PathProcessor\InboundPathProcessorInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Path processor to allow an entity's path to be passed as a single parameter.
 */
class EntityLoadPathProcessor implements InboundPathProcessorInterface {

  /**
   * {@inheritdoc}
   */
  public function processInbound($path, Request $request) {
    if (strpos($path, '/colorbox-entity-display/entity-load/') === 0) {
      $entity_path = preg_replace('|^\/colorbox-entity-display\/entity-load\/|', '', $path);

      // Replace '/' with ':' in entity path so the path can be one parameter.
      $entity_path = str_replace('/', ':', $entity_path);

      return "/colorbox-entity-display/entity-load/$entity_path";
    }
    return $path;
  }

}
